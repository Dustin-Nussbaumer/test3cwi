package Musicbox;

public class Disk {
    private String name;
    private int playTime;
    private int size = 1;


    public Disk(String name, int playTime) {
        this.name = name;
        this.playTime = playTime;
    }

    public String getName() {
        return name;
    }

    public int getPlayTime() {
        return playTime;
    }

    public int getSize() {
        return size;
    }
}
