package Musicbox;

import java.util.ArrayList;
import java.util.List;

public class Libary {
    private int capacity;
    private List<Disk> disks;

    public Libary(int capacity) {
        this.capacity = capacity;
        this.disks = new ArrayList<>();
    }
    public void saveRecord(Disk disk) {
        disks.add(disk);
    }
    public void removeDisk(Disk disk) {
        disks.remove(disk);
    }

    public int getCapacity() {
        return capacity;
    }

    public List<Disk> getFiles() {
        return disks;
    }

    public int getFreeSpace() {
        int size = 0;
        for (Disk disk : disks) {
            size += disk.getSize();
        }
        return capacity - size;
    }
}
