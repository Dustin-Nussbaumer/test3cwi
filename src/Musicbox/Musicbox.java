package Musicbox;

import java.util.List;
import java.util.Scanner;

public class Musicbox {
    private Player player;
    private Libary lib;
    Scanner scan = new Scanner(System.in);



    public Musicbox(Player player, Libary lib) {
        this.player = player;
        this.lib = lib;
    }
    public void addRecord(String name){
        Disk disk = this.player.addRecord(name);
        this.lib.saveRecord(disk);
    }
    public void removeDisk(String name){
        Disk disk = this.player.removeRecord(name);
        this.lib.removeDisk(disk);
    }

    public List<Disk> getAllFiles(){
        return this.lib.getFiles();
    }
    public int getFreeSpace(){
        return this.lib.getFreeSpace();
    }


}
